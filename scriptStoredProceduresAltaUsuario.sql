
CREATE VIEW vista_buscarUsuario

 AS

 SELECT 
		
	u.idUsuario,
	u.activo,
	u.codTipo,
	tipoUsuario.nombre as descripcionTipoUsuario,
	p.idPersona,
	p.nombre as nombreEmpleado,
	p.apellido,
	p.cuil,
	d.idDomicilio,
	d.calle,
	d.altura,
	d.piso,
	d.depto,
	t.idTelefono,
	t.codArea,
	t.numero,
	l.codLocalidad,
	l.nombreLocalidad,
	pr.codProvincia,
	pr.nombreProvincia
FROM usuario AS u

INNER JOIN tipoUsuario  ON u.codTipo = tipoUsuario.codTipo
INNER JOIN persona AS p ON p.idPersona = u.idPersona
INNER JOIN domicilio AS d ON p.idDomicilio = d.idDomicilio
INNER JOIN telefono as t ON p.idTelefono = t.idTelefono
INNER JOIN localidad as l ON d.codLocalidad = l.codLocalidad
INNER JOIN provincia AS pr ON pr.codProvincia = l.codProvincia

GO

CREATE PROCEDURE sp_buscarUsuario
@pCodTipo INT,
@pLegajo INT,
@pActivo BIT,
@pNombre VARCHAR(20)

AS

select * from vista_buscarUsuario

WHERE 1 = 1
AND (@pCodTipo = 0 OR codTipo = @pCodTipo )
AND (@pLegajo = 0 OR idUsuario = @pLegajo)
AND (activo = @pActivo)
AND (@pNombre = ''  OR nombreEmpleado LIKE @pNombre)

GO

CREATE PROCEDURE sp_modificarUsuario
@pCodTipo int,
@pActivo BIT,
@pIdPersona INT,
@pCuil varchar(11),
@pNombre varchar(20),
@pApellido varchar(20),
@pIdDomicilio INT,
@pCalle varchar(11),
@pAltura int,
@pPiso int,          
@pDepto varchar(5),
@pCodLocalidad int,
@pIdTelefono INT,
@pCodArea varchar(10),
@pNumero varchar(10),
@pIdUsuario INT
AS

UPDATE usuario


SET 

usuario.codTipo = @pCodTipo,
usuario.activo = @pActivo

WHERE 

usuario.idUsuario = @pIdUsuario

UPDATE persona

SET

persona.nombre = @pNombre,
persona.apellido = @pApellido,
persona.cuil = @pCuil

WHERE

persona.idPersona = @pIdPersona

UPDATE domicilio 

SET

domicilio.calle = @pCalle,
domicilio.altura = @pAltura,
domicilio.piso = @pPiso,
domicilio.depto = @pDepto,
domicilio.codLocalidad = @pCodLocalidad

WHERE

idDomicilio = @pIdDomicilio

UPDATE telefono 

SET

telefono.codArea = @pCodArea,
telefono.numero = @pNumero

WHERE idTelefono = @pIdTelefono;

GO



CREATE PROCEDURE sp_altaDomicilio

	@pCalle varchar(11),
	@pAltura int,
	@pPiso int,          
	@pDepto varchar(5),
	@pCodLocalidad int
	
AS
	INSERT INTO domicilio(calle,altura,piso,depto,codLocalidad)
	VALUES(@pCalle,@pAltura,@pPiso,@pDepto,@pCodLocalidad)

	SELECT SCOPE_IDENTITY()
GO

CREATE PROCEDURE sp_altaTelefono

	@pCodArea varchar(10),
	@pNumero varchar(10)
	
AS
	INSERT INTO telefono(codArea,numero)
	VALUES(@pCodArea,@pNumero)

	SELECT SCOPE_IDENTITY()
GO
	
CREATE PROCEDURE sp_altaPersona

	@pCuil varchar(11),
	@pNombre varchar(20),
	@pApellido varchar(20),
	@pIdDomicilio int,
	@pIdTelefono int

	
	
AS
	INSERT INTO persona(cuil,nombre,apellido,idDomicilio,idTelefono)
	VALUES(@pCuil,@pNombre,@pApellido,@pIdDomicilio,@pIdTelefono)

	SELECT SCOPE_IDENTITY()
GO

CREATE PROCEDURE sp_altaUsuario

	@pPass varchar(11),
	@pActivo bit,
	@pCodTipo int,
	@pIdPersona int
	
AS
	INSERT INTO usuario(pass,activo,codTipo,idPersona)
	VALUES(@pPass,@pActivo,@pCodTipo,@pIdPersona)

	SELECT SCOPE_IDENTITY()

GO

CREATE PROCEDURE sp_obtenerUsuarioLogueado

	@pLegajo int

AS
	
	SELECT 
		usuario.activo,
		tipoUsuario.codTipo AS codigoTipoUsuario,
		tipoUsuario.nombre AS descripcionTipoUsuario,
		persona.cuil AS cuilPersona, 
		persona.nombre AS nombrePersona,
		persona.apellido AS apellidoPersona 
	FROM 
		usuario
		INNER JOIN persona ON persona.idPersona = usuario.idPersona
		INNER JOIN tipoUsuario ON tipoUsuario.codTipo = usuario.codTipo
	WHERE 
		usuario.idUsuario = @pLegajo
		
	
