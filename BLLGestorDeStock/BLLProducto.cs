﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALGestorDeStock;

namespace BLLGestorDeStock
{
    public class BLLProducto : Entity.Producto
    {
		public bool RegistrarProducto()
		{
			DALProducto objDALProducto = new DALProducto();
			if (objDALProducto.Alta( this ) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool ModificarProducto()
		{
			DALProducto objDALProducto = new DALProducto();
			if (objDALProducto.Modificacion(this) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public DataTable BuscarPorParametros(string[] param)
		{
			DataTable reporte = new DataTable();
			DALProducto datos = new DALProducto();
			reporte = datos.BuscarPorParametros(param);

			return reporte;
		}



	}
	
}
