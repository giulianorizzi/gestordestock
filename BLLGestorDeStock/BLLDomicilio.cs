﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLGestorDeStock
{
    class BLLDomicilio
    {
        private BLLLocalidad objLocalidad;
        private string calle;
        private int altura;
        private int piso;
        private string depto;
        public string Calle
        {
            set { this.calle = value; }
            get { return this.calle; }
        }
        public int Altura
        {
            set { this.altura = value; }
            get { return this.altura; }
        }
        public int Piso
        {
            set { this.piso = value; }
            get { return this.piso; }
        }
        public string Depto
        {
            set { this.depto = value; }
            get { return this.depto; }
        }
    }


}
