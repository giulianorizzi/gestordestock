﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALGestorDeStock;

namespace BLLGestorDeStock
{
    public class BLLTipoProducto : Entity.TipoProducto
    { 
        public List<Entity.TipoProducto> Listar()
        {
            return (new DALGestorDeStock.DALTipoProducto()).Listar();
        }
    }
}
