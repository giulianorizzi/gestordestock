﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALGestorDeStock;
using Entity;
namespace BLLGestorDeStock
{
    public class BLLUsuario : Entity.Usuario    {

        public const int OK = 0;
        public const int ERR_NOMBRE = 1;
        public const int ERR_APELLIDO = 2;
        public const int ERR_CUIL = 3;
        public const int ERR_COD_AREA = 4;
        public const int ERR_NUM_TEL = 5;
        public const int ERR_CALLE = 6;
        public const int ERR_NUM_CALLE = 7;
        public const int ERR_PISO = 8;
        public const int ERR_LOC = 9;
        public const int ERR_TIPO = 10;


        public static Entity.Usuario login(string pass, int legajo)
        {
            DALUsuario usuario = new DALUsuario();
            Entity.Usuario usuarioBLL = new Entity.Usuario();
            usuarioBLL = usuario.login(legajo, pass);

            return usuarioBLL;
        }
        
        public int validarUsuario()
        {
            if (!validarNombre())
            {
                return ERR_NOMBRE;
            }
            else if (!validarApellido())
            {
                return ERR_APELLIDO;
            }
            else if (!validarCUIL())
            {
                return ERR_CUIL;
            }
            else if (!validarCodArea())
            {
                return ERR_COD_AREA;
            }
            else if (!validarNumero())
            {
                return ERR_NUM_TEL;
            }
            else if (!validarCalle())
            {
                return ERR_CALLE;
            }
            else if (!validarAltura())
            {
                return ERR_NUM_CALLE;
            }
            else if (!validarPiso())
            {
                return ERR_PISO;
            }
            else if (this.Domicilio.Localidad.Codigo == -1)
            {
                return ERR_LOC;
            }
            else if (this.TipoUsuario.Codigo == -1)
            {
                return ERR_TIPO;
            }
            else
            {
                return OK;
            }
        }
        public int registrarUsuario()
        {
            int validacion = validarUsuario();
            if (validacion == OK) 
            
            {
                DALUsuario objDALUsuario = new DALUsuario();
                Random random = new Random();
                this.Pass = random.Next(1000, 9999).ToString();
                this.Legajo = objDALUsuario.registrarUsuario(this);
                return OK;
            }

            else
            {
                return validacion;
            }

        }
        public int modificarUsuario()
        {
            int validacion = validarUsuario();
            if (validacion == OK)

            {
                DALUsuario objDALUsuario = new DALUsuario();
                objDALUsuario.modificarUsuario(this);
                return OK;
            }

            else
            {
                return validacion;
            }
        }
        public List<Usuario> buscarUsuario(FiltrosDeBusquedaDeUsuario pFiltros)
        {
            DALUsuario objDALUsuario = new DALUsuario();

            return objDALUsuario.buscarUsuario(pFiltros);
     
        }

        public bool validarNombre()
        {
            if (string.IsNullOrWhiteSpace(this.Nombre))
            {
                return false;
            }
            else if(this.Nombre.Replace(" ", "").Length < 2)
            {
                return false;
            }
            else if (!isAllLetters(this.Nombre))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarApellido()
        {
            if (string.IsNullOrWhiteSpace(this.Apellido))
            {
                return false;
            }
            else if (this.Apellido.Replace(" ", "").Length < 2)
            {
                return false;
            }
            else if (!isAllLetters(this.Apellido))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarCUIL()
        {
            if (string.IsNullOrWhiteSpace(this.Cuil))
            {
                return false;
            }
            else if (this.Cuil.Length != 11)
            {
                return false;
            }
            else if (!isAllNumbers(this.Cuil))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarCodArea()
        {
            if (string.IsNullOrWhiteSpace(this.Telefono.CodArea))
            {
                return false;
            }
            else if (this.Telefono.CodArea.Length > 4)
            {
                return false;
            }
            else if (!isAllNumbers(this.Telefono.CodArea))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarNumero()
        {
            if (string.IsNullOrWhiteSpace(this.Telefono.Numero))
            {
                return false;
            }
            else if (this.Telefono.Numero.Length > 10)
            {
                return false;
            }
            else if (this.Telefono.Numero.Length < 6)
            {
                return false;
            }
            else if (!isAllNumbers(this.Telefono.Numero))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarCalle()
        {
            if (string.IsNullOrWhiteSpace(this.Domicilio.Calle))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarAltura()
        {
            if (this.Domicilio.Altura < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarPiso()
        {
            if (this.Domicilio.Piso < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool isAllLetters(string cadena)
        {
            foreach (char letra in cadena.Replace(" ",""))
            {
                if (!char.IsLetter(letra))
                {
                    return false;
                }
            }
            return true;
        }
        private bool isAllNumbers(string cadena)
        {
            foreach (char letra in cadena)
            {
                if (!char.IsNumber(letra))
                {
                    return false;
                }
            }
            return true;
        }

        public BLLUsuario copiar()
        {
            BLLUsuario copiaBLLUsuario = new BLLUsuario();
            copiaBLLUsuario.Nombre = this.Nombre;
            copiaBLLUsuario.Apellido = this.Apellido;
            copiaBLLUsuario.Cuil = this.Cuil;
            copiaBLLUsuario.Activo = this.Activo;
            copiaBLLUsuario.Domicilio = this.Domicilio;
            copiaBLLUsuario.Telefono = this.Telefono;
            copiaBLLUsuario.TipoUsuario = this.TipoUsuario;
            copiaBLLUsuario.Legajo = this.Legajo;
            copiaBLLUsuario.Pass = this.Pass;

            return copiaBLLUsuario;
        }
    }



}
