﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLGestorDeStock
{
    public class BLLEstadoOrden : Entity.EstadoOrden
    {
        public List<Entity.EstadoOrden> Listar()
        {
            return (new DALGestorDeStock.DALEstadoOrden()).Listar();
        }
    }
}
