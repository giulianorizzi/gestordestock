﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class OrdenDeCompra
    {
		private int numeroOrden;
		private DateTime fecha;
		private Proveedor proveedor;
		private EstadoOrden estado;


		public EstadoOrden Estado
		{
			get { return estado; }
			set { estado = value; }
		}


		public Proveedor Proveedor
		{
			get { return proveedor; }
			set { proveedor = value; }
		}


		public DateTime Fecha
		{
			get { return fecha; }
			set { fecha = value; }
		}


		public int NumeroOrden
		{
			get { return numeroOrden; }
			set { numeroOrden = value; }
		}

	}
}
