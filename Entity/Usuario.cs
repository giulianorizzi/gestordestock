﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Usuario : Persona
    {

        private string pass;
        private int legajo;
        private TipoUsuario tipoUsuario;
        private bool activo;

        public bool Activo
        {
            get { return activo; }
            set { activo = value; }
        }
        public TipoUsuario TipoUsuario
        {
            get { return tipoUsuario; }
            set { tipoUsuario = value; }
        }
        public int Legajo        
        {
            set { this.legajo = value; }
            get { return this.legajo; }
        }
        public string Pass
        {
            set { this.pass = value; }
            get { return this.pass; }
        }
    }
}
