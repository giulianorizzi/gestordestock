﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TipoProducto
    {
		private int codigo;
		private string descripcion;

		public string Descripcion
		{
			get { return descripcion; }
			set { descripcion = value; }
		}


		public int Codigo
		{
			get { return codigo; }
			set { codigo = value; }
		}

	}
}
