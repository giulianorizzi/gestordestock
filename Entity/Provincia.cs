﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Provincia
    {
        private int codigo;
        private string nombre;
        private List<Localidad> localidades = new List<Localidad>();
       

        public List<Localidad> Localidades
        {
            get {
              
                return localidades; }
            set { localidades = value; }
        }

        public string Nombre
        {
            set
            {
                this.nombre = value;
            }
            get
            {
                return this.nombre;
            }
        }
        public int Codigo
        {
            set
            {
                this.codigo = value;
            }
            get
            {
                return this.codigo;
            }
        }
    }
}
