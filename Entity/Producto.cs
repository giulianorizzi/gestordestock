﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Producto
    {
		private int codiigo;
		private string nombre;
		private string descripcion;
		private bool activo;
		private TipoProducto tipoProducto;

		public TipoProducto TipoDeProducto
		{
			get { return tipoProducto; }
			set { tipoProducto = value; }
		}



		public bool Activo
		{
			get { return activo; }
			set { activo = value; }
		}


		public string Descripcion
		{
			get { return descripcion; }
			set { descripcion = value; }
		}


		public string  Nombre
		{
			get { return nombre; }
			set { nombre = value; }
		}


		public int Codigo
		{
			get { return codiigo; }
			set { codiigo = value; }
		}

	}
}
