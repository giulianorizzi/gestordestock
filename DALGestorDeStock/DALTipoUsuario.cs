﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
namespace DALGestorDeStock
{
    public class DALTipoUsuario : TipoUsuario
    {
        public static List<TipoUsuario> listar()
        {
            Conexion objConexion = new Conexion();
            DataTable datos = objConexion.LeerPorComando("SELECT * FROM tipoUsuario");
            List<TipoUsuario> listaTipoUsuario = new List<TipoUsuario>();

            TipoUsuario unTipoUsuario = new TipoUsuario();

            foreach (DataRow fila in datos.Rows)
            {
                unTipoUsuario = new TipoUsuario();
                unTipoUsuario.Codigo = int.Parse(fila["codTipo"].ToString());
                unTipoUsuario.Descripcion = fila["nombre"].ToString();
                listaTipoUsuario.Add(unTipoUsuario);
            }
            
            return listaTipoUsuario;
        }
    }
}
