﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALGestorDeStock
{
   public class DALUsuario
    {
        public Entity.Usuario login(int legajo, string pass)
        {
            Conexion conexion = new Conexion();
            string consulta = "SELECT * FROM usuario WHERE idUsuario = " + legajo + " AND pass = '" + pass + "'";
            DataTable datos = conexion.LeerPorComando(consulta);
            
            //si la consulta arroja resultados el pass y idUsuario son correctos
            if(datos.Rows.Count == 1)
            {

                FiltrosDeBusquedaDeUsuario filtros = new FiltrosDeBusquedaDeUsuario();
                filtros.Legajo = legajo;

                return buscarUsuario(filtros)[0];
            }
            else
            {
                return null;
            }
            

        }

        public int registrarUsuario(Usuario pUsuario)
        {
            Conexion conexion = new Conexion();

            SqlParameter[] paramSp = new SqlParameter[5];
            paramSp[0] = conexion.crearParametro("@pCalle", pUsuario.Domicilio.Calle);
            paramSp[1] = conexion.crearParametro("@pAltura", pUsuario.Domicilio.Altura);
            paramSp[2] = conexion.crearParametro("@pPiso", pUsuario.Domicilio.Piso);
            paramSp[3] = conexion.crearParametro("@pDepto", pUsuario.Domicilio.Depto);
            paramSp[4] = conexion.crearParametro("@pCodLocalidad", pUsuario.Domicilio.Localidad.Codigo);
            DataTable datos = conexion.LeerPorStoreProcedure("sp_altaDomicilio", paramSp);
         
            int idDomicilio = int.Parse(datos.Rows[0][0].ToString());
            paramSp = new SqlParameter[2];
            paramSp[0] = conexion.crearParametro("@pCodArea", pUsuario.Telefono.CodArea);
            paramSp[1] = conexion.crearParametro("@pNumero", pUsuario.Telefono.Numero);
            datos = conexion.LeerPorStoreProcedure("sp_altaTelefono", paramSp);
           
            int idTelefono = int.Parse(datos.Rows[0][0].ToString());
            paramSp = new SqlParameter[5];
            paramSp[0] = conexion.crearParametro("@pNombre", pUsuario.Nombre);
            paramSp[1] = conexion.crearParametro("@pApellido", pUsuario.Apellido);
            paramSp[2] = conexion.crearParametro("@pCuil", pUsuario.Cuil);
            paramSp[3] = conexion.crearParametro("@pIdDomicilio", idDomicilio);
            paramSp[4] = conexion.crearParametro("@pIdTelefono", idTelefono);
            datos = conexion.LeerPorStoreProcedure("sp_altaPersona", paramSp);
            
            int idPersona = int.Parse(datos.Rows[0][0].ToString());
            paramSp = new SqlParameter[4];
            paramSp[0] = conexion.crearParametro("@pPass", pUsuario.Pass);
            paramSp[1] = conexion.crearParametro("@pActivo", 1);
            paramSp[2] = conexion.crearParametro("@pCodTipo", pUsuario.TipoUsuario.Codigo);
            paramSp[3] = conexion.crearParametro("@pIdPersona", idPersona);
            datos = conexion.LeerPorStoreProcedure("sp_altaUsuario", paramSp);
            pUsuario.Legajo = int.Parse(datos.Rows[0][0].ToString());
            return pUsuario.Legajo;
        }
        public int modificarUsuario(Usuario pUsuario)
        {
            Conexion conexion = new Conexion();

            SqlParameter[] paramSp = new SqlParameter[16];
            paramSp[0] = conexion.crearParametro("@pIdDomicilio", pUsuario.Domicilio.IdDomicilio);
            paramSp[1] = conexion.crearParametro("@pCalle", pUsuario.Domicilio.Calle);
            paramSp[2] = conexion.crearParametro("@pAltura", pUsuario.Domicilio.Altura);
            paramSp[3] = conexion.crearParametro("@pPiso", pUsuario.Domicilio.Piso);
            paramSp[4] = conexion.crearParametro("@pDepto", pUsuario.Domicilio.Depto);
            paramSp[5] = conexion.crearParametro("@pCodLocalidad", pUsuario.Domicilio.Localidad.Codigo);
            paramSp[6] = conexion.crearParametro("@pIdTelefono", pUsuario.Telefono.IdTelefono);
            paramSp[7] = conexion.crearParametro("@pCodArea", pUsuario.Telefono.CodArea);
            paramSp[8] = conexion.crearParametro("@pNumero", pUsuario.Telefono.Numero);
            paramSp[9] = conexion.crearParametro("@pIdPersona", pUsuario.IdPersona);
            paramSp[10] = conexion.crearParametro("@pNombre", pUsuario.Nombre);
            paramSp[11] = conexion.crearParametro("@pApellido", pUsuario.Apellido);
            paramSp[12] = conexion.crearParametro("@pCuil", pUsuario.Cuil);
            paramSp[13] = conexion.crearParametro("@pIdUsuario", pUsuario.Legajo);
            paramSp[14] = conexion.crearParametro("@pActivo", pUsuario.Activo);
            paramSp[15] = conexion.crearParametro("@pCodTipo", pUsuario.TipoUsuario.Codigo);
            conexion.LeerPorStoreProcedure("sp_modificarUsuario", paramSp);
            return 1;
        }
        public List<Usuario> buscarUsuario(FiltrosDeBusquedaDeUsuario pFiltros)
        {
            List<Usuario> listaUsuarios = new List<Usuario>();
            Conexion conexion = new Conexion();
            Usuario unUsuario;
            SqlParameter[] paramSp = new SqlParameter[4];
            paramSp[0] = conexion.crearParametro("@pActivo", pFiltros.Estado);
            paramSp[1] = conexion.crearParametro("@pNombre", pFiltros.CadenaContenidaEnNombreOApellido);
            paramSp[2] = conexion.crearParametro("@pLegajo", pFiltros.Legajo);
            paramSp[3] = conexion.crearParametro("@pCodTipo", pFiltros.TipoUsuario.Codigo);
            DataTable datos = conexion.LeerPorStoreProcedure("sp_buscarUsuario", paramSp);

            foreach (DataRow fila in datos.Rows)
            {
                unUsuario = new Usuario();
                unUsuario.Activo = Boolean.Parse(fila["activo"].ToString());
                unUsuario.IdPersona = int.Parse(fila["idPersona"].ToString());
                unUsuario.Nombre = fila["nombreEmpleado"].ToString();
                unUsuario.Apellido = fila["apellido"].ToString();
                unUsuario.Cuil = fila["cuil"].ToString();
                unUsuario.Legajo = int.Parse(fila["idUsuario"].ToString());
                TipoUsuario unTipoUsuario = new TipoUsuario();
                unTipoUsuario.Descripcion = fila["descripcionTipoUsuario"].ToString();
                unTipoUsuario.Codigo = int.Parse(fila["codTipo"].ToString());
                Telefono unTelefono = new Telefono();
                unTelefono.IdTelefono = int.Parse(fila["idTelefono"].ToString());
                unTelefono.CodArea = fila["codArea"].ToString();
                unTelefono.Numero = fila["numero"].ToString();
                Domicilio unDomicilio = new Domicilio();
                unDomicilio.IdDomicilio = int.Parse(fila["idDomicilio"].ToString());
                unDomicilio.Calle = fila["calle"].ToString();
                unDomicilio.Altura = int.Parse(fila["altura"].ToString());
                Localidad unaLocalidad = new Localidad();
                unaLocalidad.Codigo = int.Parse(fila["codLocalidad"].ToString());
                unaLocalidad.Nombre = fila["nombreLocalidad"].ToString();
                unaLocalidad.Provincia = new Provincia() { Codigo = int.Parse(fila["codProvincia"].ToString()), Nombre = fila["nombreProvincia"].ToString() };
                unDomicilio.Localidad = unaLocalidad;
                unUsuario.Domicilio = unDomicilio;
                unUsuario.TipoUsuario = unTipoUsuario;
                unUsuario.Telefono = unTelefono;
                listaUsuarios.Add(unUsuario);
            }

            return listaUsuarios;
        }
    }
}
