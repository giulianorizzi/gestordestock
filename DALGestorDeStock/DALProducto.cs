﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALGestorDeStock 
{
    public class DALProducto : Entity.Producto
    {
        public int Alta(Entity.Producto pProducto)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[3];
            parametros[0] = objConexion.crearParametro("@pNombre", pProducto.Nombre);
            parametros[1] = objConexion.crearParametro("@pDescripcion", pProducto.Descripcion);
            parametros[2] = objConexion.crearParametro("@pCodCategoria", pProducto.TipoDeProducto.Codigo);

            return objConexion.EscribirPorStoreProcedure("sp_altaProducto", parametros);
        }

        public int Modificacion(Entity.Producto pProducto)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[5];
            parametros[0] = objConexion.crearParametro("@pCodProducto", pProducto.Codigo);
            parametros[1] = objConexion.crearParametro("@pNombre", pProducto.Nombre);
            parametros[2] = objConexion.crearParametro("@pDescripcion", pProducto.Descripcion);
            parametros[3] = objConexion.crearParametro("@pActivo", pProducto.Activo);
            parametros[4] = objConexion.crearParametro("@pCodCategoria", pProducto.TipoDeProducto.Codigo);

            return objConexion.EscribirPorStoreProcedure("sp_modificacionProducto", parametros);
        }


        public DataTable BuscarPorParametros(string[] param)
        {
            Conexion objConexion = new Conexion();
            //return objConexion.EscribirPorComando("insert into tProducto (DESCRIPCION, ID_TIPO) values ('" + pDescripcion + "', '" + pTipo + "')");
            objConexion = new Conexion();

            SqlParameter[] parametros = new SqlParameter[4];
            parametros[0] = objConexion.crearParametro("@pNombreOCodigo", param[0]);
            parametros[1] = objConexion.crearParametro("@pCategoria", param[1]);
            parametros[2] = objConexion.crearParametro("@pEstado", param[2]);
            parametros[3] = objConexion.crearParametro("@pStock", param[3]);
            DataTable datos = objConexion.LeerPorStoreProcedure("sp_verProductos", parametros);
            return datos;
        }
        

    }
}
