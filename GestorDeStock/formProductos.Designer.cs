﻿namespace GestorDeStock
{
    partial class formProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grillaProductos = new System.Windows.Forms.DataGridView();
            this.btnVolver = new System.Windows.Forms.Button();
            this.btnMostrarFormModificarProducto = new System.Windows.Forms.Button();
            this.btnMostrarFormAltaProducto = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodigoNombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCategoria = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbEstadoProducto = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStockMinimo = new System.Windows.Forms.TextBox();
            this.btnVerDetalle = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductos)).BeginInit();
            this.SuspendLayout();
            // 
            // grillaProductos
            // 
            this.grillaProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaProductos.Location = new System.Drawing.Point(32, 68);
            this.grillaProductos.Name = "grillaProductos";
            this.grillaProductos.ReadOnly = true;
            this.grillaProductos.Size = new System.Drawing.Size(643, 317);
            this.grillaProductos.TabIndex = 14;
            this.grillaProductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grillaProductos_CellContentClick);
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(563, 403);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(112, 31);
            this.btnVolver.TabIndex = 13;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnMostrarFormModificarProducto
            // 
            this.btnMostrarFormModificarProducto.Location = new System.Drawing.Point(269, 402);
            this.btnMostrarFormModificarProducto.Name = "btnMostrarFormModificarProducto";
            this.btnMostrarFormModificarProducto.Size = new System.Drawing.Size(104, 31);
            this.btnMostrarFormModificarProducto.TabIndex = 12;
            this.btnMostrarFormModificarProducto.Text = "Modificar";
            this.btnMostrarFormModificarProducto.UseVisualStyleBackColor = true;
            this.btnMostrarFormModificarProducto.Click += new System.EventHandler(this.btnMostrarFormModificarProducto_Click);
            // 
            // btnMostrarFormAltaProducto
            // 
            this.btnMostrarFormAltaProducto.Location = new System.Drawing.Point(150, 402);
            this.btnMostrarFormAltaProducto.Name = "btnMostrarFormAltaProducto";
            this.btnMostrarFormAltaProducto.Size = new System.Drawing.Size(107, 32);
            this.btnMostrarFormAltaProducto.TabIndex = 11;
            this.btnMostrarFormAltaProducto.Text = "Nuevo";
            this.btnMostrarFormAltaProducto.UseVisualStyleBackColor = true;
            this.btnMostrarFormAltaProducto.Click += new System.EventHandler(this.btnMostrarFormAltaProducto_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(540, 31);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(135, 23);
            this.btnBuscar.TabIndex = 10;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(126, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Buscar por codigo o nombre";
            // 
            // txtCodigoNombre
            // 
            this.txtCodigoNombre.Location = new System.Drawing.Point(129, 33);
            this.txtCodigoNombre.Name = "txtCodigoNombre";
            this.txtCodigoNombre.Size = new System.Drawing.Size(393, 20);
            this.txtCodigoNombre.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(698, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Categoria";
            // 
            // cbCategoria
            // 
            this.cbCategoria.FormattingEnabled = true;
            this.cbCategoria.Location = new System.Drawing.Point(701, 84);
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Size = new System.Drawing.Size(121, 21);
            this.cbCategoria.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(698, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Estado del Producto";
            // 
            // cbEstadoProducto
            // 
            this.cbEstadoProducto.FormattingEnabled = true;
            this.cbEstadoProducto.Location = new System.Drawing.Point(701, 142);
            this.cbEstadoProducto.Name = "cbEstadoProducto";
            this.cbEstadoProducto.Size = new System.Drawing.Size(121, 21);
            this.cbEstadoProducto.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(698, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Ver productos con Stock menor a";
            // 
            // txtStockMinimo
            // 
            this.txtStockMinimo.Location = new System.Drawing.Point(701, 202);
            this.txtStockMinimo.Name = "txtStockMinimo";
            this.txtStockMinimo.Size = new System.Drawing.Size(121, 20);
            this.txtStockMinimo.TabIndex = 20;
            this.txtStockMinimo.Text = "100";
            this.txtStockMinimo.TextChanged += new System.EventHandler(this.txtStockMinimo_TextChanged);
            // 
            // btnVerDetalle
            // 
            this.btnVerDetalle.Location = new System.Drawing.Point(701, 362);
            this.btnVerDetalle.Name = "btnVerDetalle";
            this.btnVerDetalle.Size = new System.Drawing.Size(75, 23);
            this.btnVerDetalle.TabIndex = 21;
            this.btnVerDetalle.Text = "Ver Detalle";
            this.btnVerDetalle.UseVisualStyleBackColor = true;
            this.btnVerDetalle.Click += new System.EventHandler(this.btnVerDetalle_Click);
            // 
            // formProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 469);
            this.Controls.Add(this.btnVerDetalle);
            this.Controls.Add(this.txtStockMinimo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbEstadoProducto);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbCategoria);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.grillaProductos);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnMostrarFormModificarProducto);
            this.Controls.Add(this.btnMostrarFormAltaProducto);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCodigoNombre);
            this.Name = "formProductos";
            this.Text = "Productos";
            this.Load += new System.EventHandler(this.formProductos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grillaProductos;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Button btnMostrarFormModificarProducto;
        private System.Windows.Forms.Button btnMostrarFormAltaProducto;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodigoNombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCategoria;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbEstadoProducto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtStockMinimo;
        private System.Windows.Forms.Button btnVerDetalle;
    }
}