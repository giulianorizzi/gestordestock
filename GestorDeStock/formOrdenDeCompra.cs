﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
namespace GestorDeStock
{
    public partial class formOrdenDeCompra : Form
    {
        Entity.Usuario usuarioUI;
        public formOrdenDeCompra(Entity.Usuario pUsuarioUI)
        {
            this.usuarioUI = pUsuarioUI;
            InitializeComponent();
        }

        private void btnVerDetalle_Click(object sender, EventArgs e)
        {
            formDetalleOrdenDeCompra ObjFormDetalleOrdenDeCompra = new formDetalleOrdenDeCompra();
            ObjFormDetalleOrdenDeCompra.Show();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            formPrincipal ObjFormPrincipal = new formPrincipal(usuarioUI);
            //ObjFormPrincipal.Show();
            this.Hide();
        }

        private void formOrdenDeCompra_Load(object sender, EventArgs e)
        {

        }
    }
}
