﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
using Entity;
namespace GestorDeStock
{
    public partial class formAltaUsuario : Form
    {

        BLLUsuario objBLLUsuario = null;
        Usuario objUsuario = null;
        private int modo;
        private const int ALTA = 0;
        private const int MODIFICACION = 1;
        public formAltaUsuario()
        {
            InitializeComponent();
           
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            modo = ALTA;
        }

        public formAltaUsuario(Usuario pUsuario)
        {

            InitializeComponent();

            this.MaximizeBox = false;
            this.MinimizeBox = false;

            objUsuario = pUsuario;
            modo = MODIFICACION;

        }
        

        private void formAltaUsuario_Load(object sender, EventArgs e)
        {
            objBLLUsuario = new BLLUsuario();
            objBLLUsuario.Telefono = new Telefono();
            objBLLUsuario.Domicilio = new Domicilio();
            objBLLUsuario.Domicilio.Localidad = new Localidad();
            objBLLUsuario.TipoUsuario = new TipoUsuario();
            cbProvincia.DataSource = BLLProvincia.listar();
            cbProvincia.DisplayMember = "Nombre";
            cbProvincia.ValueMember = "Codigo";
            cbTipoUsuario.DataSource = BLLTipoUsuario.listar(new TipoUsuario() { Codigo = -1, Descripcion = "Seleccionar" });
            cbTipoUsuario.DisplayMember = "Descripcion";
            cbTipoUsuario.ValueMember = "Codigo";
            cbEstado.DisplayMember = "Texto";
            cbEstado.ValueMember = "Valor";

            var items = new[] {
                new { Texto = "activo" , Valor = true},
                new { Texto = "inactivo", Valor = false}
            };

            cbEstado.DataSource = items;

            if (modo == ALTA)
            {
                
                objBLLUsuario.Domicilio.Localidad.Codigo = -1;
                objBLLUsuario.TipoUsuario.Codigo = -1;
                objBLLUsuario.Domicilio.Piso = 0;
                objBLLUsuario.Domicilio.Depto = "0";
                cbEstado.Enabled = false;
            }
            else
            {
                txtNombre.Text = objUsuario.Nombre;
                txtApellido.Text = objUsuario.Apellido;
                txtCuil.Text = objUsuario.Cuil;
                txtCodArea.Text = objUsuario.Telefono.CodArea;
                txtNumTelefono.Text = objUsuario.Telefono.Numero;
                txtCalle.Text = objUsuario.Domicilio.Calle;
                txtNumCalle.Text = objUsuario.Domicilio.Altura.ToString();
                txtPiso.Text = objUsuario.Domicilio.Piso.ToString();
                txtDepto.Text = objUsuario.Domicilio.Depto;
                cbTipoUsuario.SelectedValue = objUsuario.TipoUsuario.Codigo;
                cbProvincia.SelectedValue = objUsuario.Domicilio.Localidad.Provincia.Codigo;
                cbLocalidad.SelectedValue = objUsuario.Domicilio.Localidad.Codigo;
                cbEstado.SelectedValue = objUsuario.Activo;
                objBLLUsuario.IdPersona = objUsuario.IdPersona;
                objBLLUsuario.Domicilio.IdDomicilio = objUsuario.Domicilio.IdDomicilio;
                objBLLUsuario.Telefono.IdTelefono = objUsuario.Telefono.IdTelefono;
                objBLLUsuario.Legajo = objUsuario.Legajo;
            }

        }

        //************ VALIDACIONES DE LOS CAMPOS AL DETECTAR CAMBIOS EN EL TEXTO *****************

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            objBLLUsuario.Nombre = txtNombre.Text;
            if (objBLLUsuario.validarNombre())
            {
                txtNombre.ForeColor = Color.Green;
                lblErrNombre.Visible = false;

            }
            else 
            {
                txtNombre.ForeColor = Color.Red;
                lblErrNombre.Visible = true;
            
            }
        }
        private void txtApellido_TextChanged(object sender, EventArgs e)
        {
            objBLLUsuario.Apellido = txtApellido.Text;
            if (objBLLUsuario.validarApellido())
            {
                txtApellido.ForeColor = Color.Green;
                lblErrApellido.Visible = false;

            }
            else
            {
                txtApellido.ForeColor = Color.Red;
                lblErrApellido.Visible = true;
               
            }
        }
        private void txtCuil_TextChanged(object sender, EventArgs e)
        {
            objBLLUsuario.Cuil = txtCuil.Text;
            if (objBLLUsuario.validarCUIL())
            {
                txtCuil.ForeColor = Color.Green;
                lblErrCUIL.Visible = false;

            }
            else
            {
                txtCuil.ForeColor = Color.Red;
                lblErrCUIL.Visible = true;

            }
        }

        private void txtCodArea_TextChanged(object sender, EventArgs e)
        {

            objBLLUsuario.Telefono.CodArea = txtCodArea.Text;
            if (objBLLUsuario.validarCodArea())
            {
                txtCodArea.ForeColor = Color.Green;
                lblErrTel.Visible = false;

            }
            else
            {
                txtCodArea.ForeColor = Color.Red;
                lblErrTel.Visible = true;

            }
        }
        private void txtNumTelefono_TextChanged(object sender, EventArgs e)
        {

            objBLLUsuario.Telefono.Numero = txtNumTelefono.Text;
            if (objBLLUsuario.validarNumero())
            {
                txtNumTelefono.ForeColor = Color.Green;
                lblErrTel.Visible = false;

            }
            else
            {
                txtNumTelefono.ForeColor = Color.Red;
                lblErrTel.Visible = true;

            }
        }

        private void txtCalle_TextChanged(object sender, EventArgs e)
        {
            objBLLUsuario.Domicilio.Calle = txtCalle.Text;
            if (objBLLUsuario.validarCalle())
            {
                txtCalle.ForeColor = Color.Green;
                lblErrDomicilio.Visible = false;

            }
            else
            {
                txtCalle.ForeColor = Color.Red;
                lblErrDomicilio.Visible = true;

            }
        }
        private void txtNumCalle_TextChanged(object sender, EventArgs e)
        {
            int altura;
            if (int.TryParse(txtNumCalle.Text, out altura))
            {
                objBLLUsuario.Domicilio.Altura = altura;
            }
            else
            {
                objBLLUsuario.Domicilio.Altura = -1;
            }
            if (objBLLUsuario.validarAltura())
            {
                txtNumCalle.ForeColor = Color.Green;
                lblErrDomicilio.Visible = false;

            }
            else
            {
                txtNumCalle.ForeColor = Color.Red;
                lblErrDomicilio.Visible = true;

            }
        }
        private void txtPiso_TextChanged(object sender, EventArgs e)
        {
            int piso;
            if (int.TryParse(txtPiso.Text, out piso))
            {
                objBLLUsuario.Domicilio.Piso = piso;
            }
            else
            {
                if (string.IsNullOrEmpty(txtPiso.Text))
                {
                    objBLLUsuario.Domicilio.Piso = 0;
                    txtPiso.Text = "0";
                    txtPiso.SelectAll();
                }
                else
                {
                    objBLLUsuario.Domicilio.Piso = -1;
                }

            }
            if (objBLLUsuario.validarPiso())
            {
                txtPiso.ForeColor = Color.Green;
                lblErrDomicilio.Visible = false;

            }
            else
            {
                txtPiso.ForeColor = Color.Red;
                lblErrDomicilio.Visible = true;

            }
        }
        private void txtDepto_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDepto.Text))
            {
                txtDepto.Text = "0";
                txtDepto.SelectAll();
            }
            objBLLUsuario.Domicilio.Depto = txtDepto.Text;

        }


        //****************** EVENTO CHANGE DE LOS COMBOS ***********************

        private void cbProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            Provincia provinciaSelecccionada = (Provincia) cbProvincia.SelectedItem;

            if (provinciaSelecccionada.Codigo != -1)
            {
                cbLocalidad.DataSource = provinciaSelecccionada.Localidades;
                cbLocalidad.DisplayMember = "Nombre";
                cbLocalidad.ValueMember = "Codigo";
            }
            else
            {
                cbLocalidad.DataSource = null;
            }
        }
        private void cbLocalidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            objBLLUsuario.Domicilio.Localidad = (Localidad)cbLocalidad.SelectedItem;
        }

        private void cbTipoUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            objBLLUsuario.TipoUsuario = (TipoUsuario)cbTipoUsuario.SelectedItem;
        }

        //***************** EVENTO CLICK DEL BOTON ACEPTAR **********************

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            int errCode;
            if (modo == ALTA)
            {
                errCode = objBLLUsuario.registrarUsuario();

                if (errCode != BLLUsuario.OK)
                {
                    MessageBox.Show("Uno o mas datos ingresados son invalidos", "Error al registrar el nuevo usuario");
                }
                else
                {
                    //SI EL USUARIO SE REGISTRO CORRECTAMENTE LIMPIO LOS CAMPOS DE TEXTO

                    MessageBox.Show("El usuario fue registrado con exito", "Registrar usuario");


                    formDetalleUsuario objFormDetalleUsuario = new formDetalleUsuario(objBLLUsuario.copiar());
                    objFormDetalleUsuario.ShowDialog(this);
                    txtNombre.ResetText();
                    txtApellido.ResetText();
                    txtCuil.ResetText();
                    txtCodArea.ResetText();
                    txtNumTelefono.ResetText();
                    txtCalle.ResetText();
                    txtNumCalle.ResetText();
                    txtPiso.ResetText();
                    txtDepto.ResetText();
                    cbTipoUsuario.SelectedValue = -1;
                    cbProvincia.SelectedValue = -1;
                    cbTipoUsuario.Focus();
                    lblErrNombre.Visible = false;
                    lblErrApellido.Visible = false;
                    lblErrCUIL.Visible = false;
                    lblErrTel.Visible = false;
                    lblErrDomicilio.Visible = false;
                }
            }
            else
            {
                errCode = objBLLUsuario.modificarUsuario();

                if (errCode != BLLUsuario.OK)
                {
                    MessageBox.Show("Uno o mas datos ingresados son invalidos", "Error al modificar el usuario");
                }
                else
                {
                    MessageBox.Show("El usuario fue modificado correctamente", "Modificar usuario");
                }
            }
        }
        //************ VALIDACIONES AL PERDER EL FOCO *************************

        private void txtNombre_LostFocus(object sender, System.EventArgs e)
        {
            if (!objBLLUsuario.validarNombre())
            {

                txtNombre.ForeColor = Color.Red;
                lblErrNombre.Visible = true;

            }
        }
        private void txtApellido_LostFocus(object sender, System.EventArgs e)
        {
            if (!objBLLUsuario.validarApellido())
            {

                txtApellido.ForeColor = Color.Red;
                lblErrApellido.Visible = true;

            }
        }
        private void txtCuil_LostFocus(object sender, System.EventArgs e)
        {
            if (!objBLLUsuario.validarCUIL())
            {

                txtCuil.ForeColor = Color.Red;
                lblErrCUIL.Visible = true;

            }
        }
        private void txtCodArea_LostFocus(object sender, System.EventArgs e)
        {
            if (!objBLLUsuario.validarCodArea())
            {

                txtCodArea.ForeColor = Color.Red;
                lblErrTel.Visible = true;

            }
        }
        private void txtNumTelefono_LostFocus(object sender, System.EventArgs e)
        {
            if (!objBLLUsuario.validarNumero())
            {

                txtNumTelefono.ForeColor = Color.Red;
                lblErrTel.Visible = true;

            }
        }
        private void txtCalle_LostFocus(object sender, System.EventArgs e)
        {
            if (!objBLLUsuario.validarCalle())
            {

                txtCalle.ForeColor = Color.Red;
                lblErrDomicilio.Visible = true;

            }
        }
        private void txtNumCalle_LostFocus(object sender, System.EventArgs e)
        {
            if (!objBLLUsuario.validarAltura())
            {

                txtNumCalle.ForeColor = Color.Red;
                lblErrDomicilio.Visible = true;

            }
        }
        private void txtPiso_LostFocus(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPiso.Text))
            {
                txtPiso.Text = "0";
            }
            if (!objBLLUsuario.validarPiso())
            {

                txtPiso.ForeColor = Color.Red;
                lblErrDomicilio.Visible = true;

            }
        }
        private void txtDepto_LostFocus(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDepto.Text))
            {
                txtDepto.Text = "0";
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void cbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbEstado.Focused)
            {
                objBLLUsuario.Activo = (bool)cbEstado.SelectedValue;
                if ((bool)cbEstado.SelectedValue == false)
                {
                    MessageBox.Show("Si cambia el estado a INACTIVO el usuario no podra iniciar sesion en el sistema \r\n si no desea que esto suceda vuelva a seleccionar la opcion de ACTIVO");
                }
                else
                {
                    MessageBox.Show("Si cambia el estado a ACTIVO el usuario podra volver a iniciar sesion en el sistema \r\n si no desea que esto suceda vuelva a seleccionar la opcion de INACTIVO");
                }
            }

        }
    }
}
