﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
namespace GestorDeStock
{
    public partial class formProductos : Form
    {

        Entity.Usuario usuarioUI;
        public formProductos(Entity.Usuario pUsuarioUI)
        {
            InitializeComponent();
        }

        private void formProductos_Load(object sender, EventArgs e)
        {
            this.CargarTiposDeProducto();
            this.CargarEstados();
           
        }

        private void btnVerDetalle_Click(object sender, EventArgs e)
        {
            formProductoDetalle ObjProductoDetalle = new formProductoDetalle();
            ObjProductoDetalle.Show();
        }

        private void btnMostrarFormAltaProducto_Click(object sender, EventArgs e)
        {
            formAltaProducto ObjAltaProducto = new formAltaProducto();
            ObjAltaProducto.Show();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            formPrincipal ObjFormPrincipal = new formPrincipal(usuarioUI);
            //ObjFormPrincipal.Show();
            this.Hide();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BLLProducto objBLLProducto = new BLLProducto();
            //Creo un array de string con los parametros de busqueda
            string[] parametros = new string[4];
            parametros[0] = txtCodigoNombre.Text;
            parametros[1] = cbCategoria.SelectedValue.ToString();
            parametros[2] = cbEstadoProducto.SelectedValue.ToString();
            parametros[3] = txtStockMinimo.Text;
            //busco
            grillaProductos.DataSource = objBLLProducto.BuscarPorParametros(parametros);
            //Oculto la columna 4 que contiene el codigo tipo producto
            grillaProductos.Columns[4].Visible = false;

        }


        private void CargarTiposDeProducto()
        {
            BLLTipoProducto objTipoProducto = new BLLTipoProducto();
            cbCategoria.DataSource = objTipoProducto.Listar();
            cbCategoria.DisplayMember = "Descripcion";
            cbCategoria.ValueMember = "Codigo";
        }

        private void CargarEstados()
        {
            //Combobox del estado
            cbEstadoProducto.DisplayMember = "Texto";
            cbEstadoProducto.ValueMember = "Valor";

            var items = new[] {
                new { Texto = "activo" , Valor = "true"},
                new { Texto = "inactivo", Valor = "false" }
            };

            cbEstadoProducto.DataSource = items;
        }

        private void btnMostrarFormModificarProducto_Click(object sender, EventArgs e)
        {
            if (grillaProductos.SelectedCells.Count > 0)
            {
                BLLProducto objProducto = new BLLProducto();
                BLLTipoProducto objTipoProducto = new BLLTipoProducto();
                objProducto.Codigo = int.Parse(grillaProductos.CurrentRow.Cells["Codigo"].Value.ToString());
                objProducto.Descripcion = grillaProductos.CurrentRow.Cells["Descripcion"].Value.ToString();
                objProducto.Nombre = grillaProductos.CurrentRow.Cells["Nombre"].Value.ToString();
                objProducto.Activo = bool.Parse(grillaProductos.CurrentRow.Cells["Estado"].Value.ToString());
                objTipoProducto.Codigo = int.Parse(grillaProductos.CurrentRow.Cells["codCategoria"].Value.ToString());
                objTipoProducto.Descripcion = grillaProductos.CurrentRow.Cells["Categoria"].Value.ToString();
                objProducto.TipoDeProducto = objTipoProducto;
                formAltaProducto ObjFormAltaProducto = new formAltaProducto(objProducto);
                ObjFormAltaProducto.Show();
            }
            else
            {
                MessageBox.Show("Primero debe seleccionar un producto");
            }
            
        }

        private void grillaProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtStockMinimo_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
