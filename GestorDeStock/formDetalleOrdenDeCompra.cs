﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorDeStock
{
    public partial class formDetalleOrdenDeCompra : Form
    {
        public formDetalleOrdenDeCompra()
        {
            InitializeComponent();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnAgregarProductoOC_Click(object sender, EventArgs e)
        {
            formAgregarProductoOrdenDeCompra ObjFormAgregarProductoOrdenDeCompra = new formAgregarProductoOrdenDeCompra();
            ObjFormAgregarProductoOrdenDeCompra.Show();
        }

        private void btnVerHistorico_Click(object sender, EventArgs e)
        {
            formHistoricoOrdenDeCompra ObjFormHistoricoOrdenDeCompra = new formHistoricoOrdenDeCompra();
            ObjFormHistoricoOrdenDeCompra.Show();
        }
    }
}
