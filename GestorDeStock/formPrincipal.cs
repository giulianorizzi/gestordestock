﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
namespace GestorDeStock
{
    public partial class formPrincipal : Form
    {
        Entity.Usuario usuarioUI;
        public formPrincipal(Entity.Usuario pUsuarioUI)
        {
            InitializeComponent();
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.usuarioUI = pUsuarioUI;
            
        }

        private void productorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formProductos ObjFormProductos = new formProductos(usuarioUI);
            ObjFormProductos.ShowDialog(this);
            //this.Hide();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formUsuarios ObjFormUsuarios = new formUsuarios(usuarioUI);
            ObjFormUsuarios.ShowDialog(this);
            //this.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            formLogin ObjFormLogin = new formLogin();
            ObjFormLogin.Show();
            this.Hide();
        }

        private void ordenDeCompraToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            formOrdenDeCompra ObjFormOrdenDeCompra = new formOrdenDeCompra(usuarioUI);
            ObjFormOrdenDeCompra.ShowDialog(this);
            //this.Hide();

        }

        private void formPrincipal_Load(object sender, EventArgs e)
        {
            
            lblNombre.Text = this.usuarioUI.Nombre + " " + this.usuarioUI.Apellido + " (" + this.usuarioUI.TipoUsuario.Descripcion + ")";
            lblCuil.Text = this.usuarioUI.Cuil;
            lblLegajo.Text = this.usuarioUI.Legajo.ToString();
            lblTelefono.Text = this.usuarioUI.Telefono.ToString();
            lblDomicilio.Text = this.usuarioUI.Domicilio.ToString();
            //Por cada menu item en nuestros menuitems
            foreach (ToolStripMenuItem item in menuStrip1.Items)
            {
                //Si el tag contiene el tipo de usuario... (Uso contiene porque algunos tags tienen varios tipos de usuarios separados por ;)
                if(item.Tag.ToString().Contains(usuarioUI.TipoUsuario.Descripcion))
                {
                    item.Visible = true;
                }
                else
                {
                    item.Visible = false;
                }
             
            }
           
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
