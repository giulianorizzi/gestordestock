﻿namespace GestorDeStock
{
    partial class formHistoricoOrdenDeCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grillaOrdenDeCompra = new System.Windows.Forms.DataGridView();
            this.btnVolver = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grillaOrdenDeCompra)).BeginInit();
            this.SuspendLayout();
            // 
            // grillaOrdenDeCompra
            // 
            this.grillaOrdenDeCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaOrdenDeCompra.Location = new System.Drawing.Point(45, 50);
            this.grillaOrdenDeCompra.Name = "grillaOrdenDeCompra";
            this.grillaOrdenDeCompra.Size = new System.Drawing.Size(715, 361);
            this.grillaOrdenDeCompra.TabIndex = 29;
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(701, 442);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(75, 23);
            this.btnVolver.TabIndex = 30;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // formHistoricoOrdenDeCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 480);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.grillaOrdenDeCompra);
            this.Name = "formHistoricoOrdenDeCompra";
            this.Text = "formHistoricoOrdenDeCompra";
            ((System.ComponentModel.ISupportInitialize)(this.grillaOrdenDeCompra)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grillaOrdenDeCompra;
        private System.Windows.Forms.Button btnVolver;
    }
}