﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
namespace GestorDeStock
{
    public partial class formLogin : Form

    {

        public formLogin()
        {

            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            

        }

        private void FormProductos_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPass_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            Entity.Usuario usuarioUI = new Entity.Usuario();
            int legajo;
            string pass;
            if (txtPass.Text != "" && txtLegajo.Text != "")
            {
                if (int.TryParse(txtLegajo.Text, out legajo))
                {
                    pass = txtPass.Text;
                    //ejecuto el login de usuario
                    usuarioUI = BLLUsuario.login(pass, legajo);
                    if (usuarioUI != null && usuarioUI.Activo)
                    {

                        //si el login fue correcto y se pudieron cargar los datos del usuario
                        formPrincipal ObjFormPrincipal = new formPrincipal(usuarioUI);
                        ObjFormPrincipal.Show();
                        this.Hide();
                    }

                    else if(usuarioUI == null)
                    {
                        MessageBox.Show("Usuario o contraseña incorrecta", "Error al iniciar sesión", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtLegajo.Text = "";
                        txtPass.Text = "";
                        txtLegajo.Focus();
                    }
                    else if (!usuarioUI.Activo)
                    {
                        MessageBox.Show("El usuario fue desactivado por un administrador","Error al iniciar sesión",MessageBoxButtons.OK,MessageBoxIcon.Error);
                        txtLegajo.Text = "";
                        txtPass.Text = "";
                        txtLegajo.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un legajo válido", "Inicio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtLegajo.Text = "";
                }
            }
            else
            {
                MessageBox.Show("Recuerde completar todos los campos", "Inicio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        
    }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
                Application.Exit();
        }
    }
}
