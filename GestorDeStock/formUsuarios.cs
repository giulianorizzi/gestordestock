﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
using Entity;

namespace GestorDeStock
{
    public partial class formUsuarios : Form
    {
        Entity.Usuario usuarioUI;
        formAltaUsuario objFormAltaUsuario;
        public formUsuarios(Entity.Usuario pUsuarioUI)
        {
            this.usuarioUI = pUsuarioUI;
            InitializeComponent();
          
        }
        
        private void formUsuarios_Load(object sender, EventArgs e)
        {
               
            /*grillaUsuarios.Columns.Add("Column", "Legajo");
            grillaUsuarios.Columns.Add("Column", "Nombre y Apellido");
            grillaUsuarios.Columns.Add("Column", "DNI");
            grillaUsuarios.Columns.Add("Column", "Tipo de usuario");
            grillaUsuarios.Columns.Add("Column", "Fecha de alta");*/

            cbTipoUsuario.DataSource = BLLTipoUsuario.listar(new TipoUsuario() { Descripcion = "Todos", Codigo = 0});
            cbEstado.DisplayMember = "Texto";
            cbEstado.ValueMember = "Valor";
            
            var items = new [] {
                new { Texto = "todos" , Valor = 2},
                new { Texto = "activo" , Valor = 1},
                new { Texto = "inactivo", Valor = 0}
            };

            cbEstado.DataSource = items;

            buscarUsuario();
            grillaUsuarios.Select();
        }

        private void btnMostrarFormModificarUsuario_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection filasSeleccionadas = grillaUsuarios.SelectedRows;

            if(filasSeleccionadas.Count > 0)
            {
                objFormAltaUsuario = new formAltaUsuario((Usuario)filasSeleccionadas[0].DataBoundItem);
                objFormAltaUsuario.ShowDialog(this);
                buscarUsuario();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un usuario de la lista", "Error", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }

        }

        private void btnMostrarFormAltaUsuario_Click(object sender, EventArgs e)
        {
            objFormAltaUsuario = new formAltaUsuario();
            objFormAltaUsuario.ShowDialog(this);
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            formPrincipal ObjFormPrincipal = new formPrincipal(usuarioUI);
            //ObjFormPrincipal.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscarUsuario();
        }

        private void cbTipoUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buscarUsuario()
        {
            FiltrosDeBusquedaDeUsuario filtros = new FiltrosDeBusquedaDeUsuario();
            BLLUsuario objBLLUsuario = new BLLUsuario();
            int legajoBuscado;
            filtros.Estado = (int)cbEstado.SelectedValue;
            filtros.TipoUsuario = (TipoUsuario)cbTipoUsuario.SelectedItem;
            if (int.TryParse(txtBusqueda.Text, out legajoBuscado))
            {
                filtros.Legajo = legajoBuscado;
                filtros.CadenaContenidaEnNombreOApellido = "";
            }
            else
            {

                filtros.CadenaContenidaEnNombreOApellido = txtBusqueda.Text;
                filtros.Legajo = FiltrosDeBusquedaDeUsuario.TODOS_LOS_LEGAJOS;

            }
            grillaUsuarios.DataSource = objBLLUsuario.buscarUsuario(filtros);
            grillaUsuarios.Columns["Legajo"].DisplayIndex = 0;
            grillaUsuarios.Columns["Cuil"].DisplayIndex = 4;
            //grillaUsuarios.Columns["Activo"].Visible = false;
            grillaUsuarios.Columns["Pass"].Visible = false;
            grillaUsuarios.Columns["Domicilio"].Visible = false;
            grillaUsuarios.Columns["TipoUsuario"].HeaderText = "Tipo";
            grillaUsuarios.Columns["idPersona"].Visible = false;
            lblResultados.Text = "Se encontraron " + grillaUsuarios.Rows.Count + " usuarios con los parametros: \r\n Estado: "
                + cbEstado.Text +
                "\r\n Tipo: " + cbTipoUsuario.Text +
                "\r\n Legajo: " + filtros.Legajo + " (Muestra todos los legajos si es 0)" +
                "\r\n Nombres que contienen: '" + filtros.CadenaContenidaEnNombreOApellido + "' (Muestra todos los nombres si es '')";
        }

        private void cbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnDetalle_Click(object sender, EventArgs e)
        {

            DataGridViewSelectedRowCollection filasSeleccionadas = grillaUsuarios.SelectedRows;

            if (filasSeleccionadas.Count > 0)
            {
                formDetalleUsuario objFormDetalleUsuario = new formDetalleUsuario((Usuario)filasSeleccionadas[0].DataBoundItem);
                objFormDetalleUsuario.ShowDialog(this);
            }
        }
    }
}
