﻿namespace GestorDeStock
{
    partial class formDetalleOrdenDeCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolver = new System.Windows.Forms.Button();
            this.grillaProductoDetalle = new System.Windows.Forms.DataGridView();
            this.lblTipoProducto = new System.Windows.Forms.Label();
            this.lblCodigoProducto = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbProveedor = new System.Windows.Forms.ComboBox();
            this.btnAprobar = new System.Windows.Forms.Button();
            this.btnAgregarProductoOC = new System.Windows.Forms.Button();
            this.btnVerHistorico = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductoDetalle)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(600, 405);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(75, 23);
            this.btnVolver.TabIndex = 21;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // grillaProductoDetalle
            // 
            this.grillaProductoDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaProductoDetalle.Location = new System.Drawing.Point(129, 49);
            this.grillaProductoDetalle.Name = "grillaProductoDetalle";
            this.grillaProductoDetalle.Size = new System.Drawing.Size(546, 317);
            this.grillaProductoDetalle.TabIndex = 20;
            // 
            // lblTipoProducto
            // 
            this.lblTipoProducto.AutoSize = true;
            this.lblTipoProducto.Location = new System.Drawing.Point(249, 22);
            this.lblTipoProducto.Name = "lblTipoProducto";
            this.lblTipoProducto.Size = new System.Drawing.Size(0, 13);
            this.lblTipoProducto.TabIndex = 19;
            // 
            // lblCodigoProducto
            // 
            this.lblCodigoProducto.AutoSize = true;
            this.lblCodigoProducto.Location = new System.Drawing.Point(183, 22);
            this.lblCodigoProducto.Name = "lblCodigoProducto";
            this.lblCodigoProducto.Size = new System.Drawing.Size(25, 13);
            this.lblCodigoProducto.TabIndex = 18;
            this.lblCodigoProducto.Text = "123";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(126, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Orden N°";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Proveedor";
            // 
            // cbProveedor
            // 
            this.cbProveedor.FormattingEnabled = true;
            this.cbProveedor.Location = new System.Drawing.Point(276, 19);
            this.cbProveedor.Name = "cbProveedor";
            this.cbProveedor.Size = new System.Drawing.Size(121, 21);
            this.cbProveedor.TabIndex = 23;
            // 
            // btnAprobar
            // 
            this.btnAprobar.Location = new System.Drawing.Point(129, 405);
            this.btnAprobar.Name = "btnAprobar";
            this.btnAprobar.Size = new System.Drawing.Size(75, 23);
            this.btnAprobar.TabIndex = 24;
            this.btnAprobar.Text = "Aprobar";
            this.btnAprobar.UseVisualStyleBackColor = true;
            // 
            // btnAgregarProductoOC
            // 
            this.btnAgregarProductoOC.Location = new System.Drawing.Point(692, 49);
            this.btnAgregarProductoOC.Name = "btnAgregarProductoOC";
            this.btnAgregarProductoOC.Size = new System.Drawing.Size(112, 23);
            this.btnAgregarProductoOC.TabIndex = 25;
            this.btnAgregarProductoOC.Text = "Agregar Producto";
            this.btnAgregarProductoOC.UseVisualStyleBackColor = true;
            this.btnAgregarProductoOC.Click += new System.EventHandler(this.btnAgregarProductoOC_Click);
            // 
            // btnVerHistorico
            // 
            this.btnVerHistorico.Location = new System.Drawing.Point(692, 91);
            this.btnVerHistorico.Name = "btnVerHistorico";
            this.btnVerHistorico.Size = new System.Drawing.Size(112, 23);
            this.btnVerHistorico.TabIndex = 26;
            this.btnVerHistorico.Text = "Ver historico";
            this.btnVerHistorico.UseVisualStyleBackColor = true;
            this.btnVerHistorico.Click += new System.EventHandler(this.btnVerHistorico_Click);
            // 
            // formDetalleOrdenDeCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 450);
            this.Controls.Add(this.btnVerHistorico);
            this.Controls.Add(this.btnAgregarProductoOC);
            this.Controls.Add(this.btnAprobar);
            this.Controls.Add(this.cbProveedor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.grillaProductoDetalle);
            this.Controls.Add(this.lblTipoProducto);
            this.Controls.Add(this.lblCodigoProducto);
            this.Controls.Add(this.label1);
            this.Name = "formDetalleOrdenDeCompra";
            this.Text = "formDetalleOrdenDeCompra";
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductoDetalle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.DataGridView grillaProductoDetalle;
        private System.Windows.Forms.Label lblTipoProducto;
        private System.Windows.Forms.Label lblCodigoProducto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbProveedor;
        private System.Windows.Forms.Button btnAprobar;
        private System.Windows.Forms.Button btnAgregarProductoOC;
        private System.Windows.Forms.Button btnVerHistorico;
    }
}