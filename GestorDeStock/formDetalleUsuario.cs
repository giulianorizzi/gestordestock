﻿using Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entity;
namespace GestorDeStock
{
    public partial class formDetalleUsuario : Form
    {
        Usuario objUsuario;
        public formDetalleUsuario(Usuario pUsuario)
        {
            objUsuario = pUsuario;
            InitializeComponent();
        }

        private void formDetalleUsuario_Load(object sender, EventArgs e)
        {
            lblLegajo.Text = objUsuario.Legajo.ToString();
            lblPass.Text = objUsuario.Pass;
            lblTipoUsuario.Text = objUsuario.TipoUsuario.Descripcion;
            lblNombre.Text = objUsuario.Nombre;
            lblApellido.Text = objUsuario.Apellido;
            lblCuil.Text = objUsuario.Cuil;
            lblDomicilio.Text = objUsuario.Domicilio.ToString();
            lblTelefono.Text = objUsuario.Telefono.ToString();

        }

        public void llenarDatos()
        {

        }
    }
}
