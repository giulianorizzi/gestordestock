USE [DBGestorDeStock]
GO
/****** Object:  StoredProcedure [dbo].[sp_modificarUsuario]    Script Date: 24/05/2020 17:38:44 ******/
DROP PROCEDURE [dbo].[sp_modificarUsuario]
GO
/****** Object:  StoredProcedure [dbo].[sp_buscarUsuario]    Script Date: 24/05/2020 17:38:44 ******/
DROP PROCEDURE [dbo].[sp_buscarUsuario]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaUsuario]    Script Date: 24/05/2020 17:38:44 ******/
DROP PROCEDURE [dbo].[sp_altaUsuario]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaTelefono]    Script Date: 24/05/2020 17:38:44 ******/
DROP PROCEDURE [dbo].[sp_altaTelefono]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaProducto]    Script Date: 24/05/2020 17:38:44 ******/
DROP PROCEDURE [dbo].[sp_altaProducto]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaPersona]    Script Date: 24/05/2020 17:38:44 ******/
DROP PROCEDURE [dbo].[sp_altaPersona]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaDomicilio]    Script Date: 24/05/2020 17:38:44 ******/
DROP PROCEDURE [dbo].[sp_altaDomicilio]
GO
/****** Object:  View [dbo].[vista_buscarUsuario]    Script Date: 24/05/2020 17:38:44 ******/
DROP VIEW [dbo].[vista_buscarUsuario]
GO
/****** Object:  View [dbo].[vista_buscarUsuario]    Script Date: 24/05/2020 17:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vista_buscarUsuario]

 AS

 SELECT 
		
	u.idUsuario,
	u.activo,
	u.codTipo,
	tipoUsuario.nombre as descripcionTipoUsuario,
	p.idPersona,
	p.nombre as nombreEmpleado,
	p.apellido,
	p.cuil,
	d.idDomicilio,
	d.calle,
	d.altura,
	d.piso,
	d.depto,
	t.idTelefono,
	t.codArea,
	t.numero,
	l.codLocalidad,
	l.nombreLocalidad,
	pr.codProvincia,
	pr.nombreProvincia
FROM usuario AS u

INNER JOIN tipoUsuario  ON u.codTipo = tipoUsuario.codTipo
INNER JOIN persona AS p ON p.idPersona = u.idPersona
INNER JOIN domicilio AS d ON p.idDomicilio = d.idDomicilio
INNER JOIN telefono as t ON p.idTelefono = t.idTelefono
INNER JOIN localidad as l ON d.codLocalidad = l.codLocalidad
INNER JOIN provincia AS pr ON pr.codProvincia = l.codProvincia



GO
/****** Object:  StoredProcedure [dbo].[sp_altaDomicilio]    Script Date: 24/05/2020 17:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_altaDomicilio]

	@pCalle varchar(11),
	@pAltura int,
	@pPiso int,
	@pDepto varchar(5),
	@pCodLocalidad int
	
AS
	INSERT INTO domicilio(calle,altura,piso,depto,codLocalidad)
	VALUES(@pCalle,@pAltura,@pPiso,@pDepto,@pCodLocalidad)

	SELECT SCOPE_IDENTITY()


GO
/****** Object:  StoredProcedure [dbo].[sp_altaPersona]    Script Date: 24/05/2020 17:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
CREATE PROCEDURE [dbo].[sp_altaPersona]

	@pCuil varchar(11),
	@pNombre varchar(20),
	@pApellido varchar(20),
	@pIdDomicilio int,
	@pIdTelefono int

	
	
AS
	INSERT INTO persona(cuil,nombre,apellido,idDomicilio,idTelefono)
	VALUES(@pCuil,@pNombre,@pApellido,@pIdDomicilio,@pIdTelefono)

	SELECT SCOPE_IDENTITY()


GO
/****** Object:  StoredProcedure [dbo].[sp_altaProducto]    Script Date: 24/05/2020 17:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_altaProducto]
	@pNombre  nvarchar(50),
	@pDescripcion  nvarchar(50),
	@pCodCategoria int
as
	INSERT INTO [dbo].[producto]([nombre],[descripcion],[codCategoria])
     VALUES(@pNombre, @pDescripcion, @pCodCategoria)


GO
/****** Object:  StoredProcedure [dbo].[sp_altaTelefono]    Script Date: 24/05/2020 17:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_altaTelefono]

	@pCodArea varchar(10),
	@pNumero varchar(10)
	
AS
	INSERT INTO telefono(codArea,numero)
	VALUES(@pCodArea,@pNumero)

	SELECT SCOPE_IDENTITY()


GO
/****** Object:  StoredProcedure [dbo].[sp_altaUsuario]    Script Date: 24/05/2020 17:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_altaUsuario]

	@pPass varchar(11),
	@pActivo bit,
	@pCodTipo int,
	@pIdPersona int
	
AS
	INSERT INTO usuario(pass,activo,codTipo,idPersona)
	VALUES(@pPass,@pActivo,@pCodTipo,@pIdPersona)

	SELECT SCOPE_IDENTITY()



GO
/****** Object:  StoredProcedure [dbo].[sp_buscarUsuario]    Script Date: 24/05/2020 17:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_buscarUsuario]
@pCodTipo INT,
@pLegajo INT,
@pActivo INT,
@pNombre VARCHAR(20)

AS

select * from vista_buscarUsuario

WHERE 1 = 1
AND (@pCodTipo = 0 OR codTipo = @pCodTipo )
AND (@pLegajo = 0 OR idUsuario = @pLegajo)
AND (@pActivo = 2 OR activo = @pActivo)
AND (@pNombre = ''  OR nombreEmpleado LIKE '%' + @pNombre + '%')



GO
/****** Object:  StoredProcedure [dbo].[sp_modificarUsuario]    Script Date: 24/05/2020 17:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_modificarUsuario]
@pCodTipo int,
@pActivo BIT,
@pIdPersona INT,
@pCuil varchar(11),
@pNombre varchar(20),
@pApellido varchar(20),
@pIdDomicilio INT,
@pCalle varchar(11),
@pAltura int,
@pPiso int,          
@pDepto varchar(5),
@pCodLocalidad int,
@pIdTelefono INT,
@pCodArea varchar(10),
@pNumero varchar(10),
@pIdUsuario INT
AS

UPDATE usuario


SET 

usuario.codTipo = @pCodTipo,
usuario.activo = @pActivo

WHERE 

usuario.idUsuario = @pIdUsuario

UPDATE persona

SET

persona.nombre = @pNombre,
persona.apellido = @pApellido,
persona.cuil = @pCuil

WHERE

persona.idPersona = @pIdPersona

UPDATE domicilio 

SET

domicilio.calle = @pCalle,
domicilio.altura = @pAltura,
domicilio.piso = @pPiso,
domicilio.depto = @pDepto,
domicilio.codLocalidad = @pCodLocalidad

WHERE

idDomicilio = @pIdDomicilio

UPDATE telefono 

SET

telefono.codArea = @pCodArea,
telefono.numero = @pNumero

WHERE idTelefono = @pIdTelefono;



GO
